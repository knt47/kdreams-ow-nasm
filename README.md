# Commander Keen Dreams, Open Watcom + NASM port

This is a "fork" of the GPL v2+ licensed source code of the 1991 video game Commander Keen Dreams, This fork can be built using Open Watcom 2.0 (or 1.9) and NASM. I have created this "source port" as an "experimental" project to study the differences between two compilers for DOS (Watcom vs. Borland C++). The main advantage of Watcom is that it allows cross-compilation for DOS on the 64-bit x86 versions of Windows and Linux without using any form of emulation. The original sources are available on GitHub: [Link](https://github.com/keendreams/keen).

The source code of this game was released thanks to a crowdfunding effort coordinated by Javier M. Chavez and Chuck Naaden.

## Notes about game data files

To play the game built from the sources, you need data files from the original game (or a "shareware" version that can be downloaded from several "DOS shareware games" websites.) These files cannot be included here for copyright reasons.

This repository also does not include the "statically linked" data files that need to be put into the STATIC directory. These files are available in the ["original" Github repository](https://github.com/keendreams/keen) for version 1.93 - or [from here](https://github.com/keendreams/keen/tree/a7591c4af15c479d8d1c0be5ce1d49940554157c) if you want compatibility with the "1.13 shareware" version. Note that you do not need to run the "make.bat" file (and will not work), just copy the files with extension .KDR into the STATIC subdirectory.

From the [GitHub repository](https://github.com/keendreams/keen):

> "The release of the source code does not affect the licensing of the game data files, which you must still legally acquire. This includes the static data included in this repository for your convenience. However, you are permitted to link and distribute that data for the purposes of compatibility with the original game."

## Build instructions

Open Watcom requires a few environment variables to be set up. For more information, see the "readme.txt" file of the compiler. The PATH environment variable must also include the path to the NASM executable. The "tools" subdirectory of this repository contains an example CMD script and shell script file for setting up the environment variables on Windows and Linux, respectively.

The program can be built by running WMAKE. There are two different Makefiles, KDRWIN.MK for Windows (and DOS) and KDRLINUX.MK for Linux. The OBJ subdirectory must be the working directory.

    wmake -f ../KDRWIN.MK
    
    
    wmake -f ../KDRLINUX.MK
    wmake -f ../KDRLINUX.MK BLD_OW19_WORKAROUND=1
        (for Open Watcom 1.9 and perhaps older versions of OW 2.0)

Note 1: The built executable file will be placed along with the objects files, into the OBJ subdirectory.
Note 2: the build was tested only for the EGA version and only with the "1.13 shareware" data files.

## License

The Commander Keen Dreams source code, as well as the files of this fork derived from it, are licensed under the GNU General Public License, version 2, or later (see src/LICENSE.TXT). The game data files are not covered by this license, see comments above.

Some source files (BIN_KDR.ASM, OWCOMPAT.H, ...) that are not from the original source release and are only present in this port, are also available under the 2-clause BSD license.

This repository includes intentionally only the source code, and contains no binary release (due to the difficulty of complying with some restrictions of the GPL license that only apply when binary files are distributed). As I have noted above, this is an "experimental" project. The point is here to demonstrate the differences between two compilers for DOS, therefore the source code itself contains the valuable information.
