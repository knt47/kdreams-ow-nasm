#/bin/sh

# Exit on error
set -e

# *** ***  Paths that need to be modified  *** ***

# The full path to the "src" directory in the downloaded source code
export OUTSRCDIR="/home/example/sources/kdreams-ow-nasm/src"

# The directory where Open Watcom was installed
export WATCOMDIR="/home/example/software/ow20"

# Change WATCOMPLATFORM to binl to use 32-bit Watcom
export WATCOMPLATFORM=binl64

# If NASM was installed in some "non-standard" location not in the PATH
#   environment variable, the path to its executable can be provided here
export NASMDIR=

# (default value)
WMAKE_ARGS=

# Uncomment the following line for Open Watcom 1.9 (and probably older 
#   versions of OW 2.1)
## WMAKE_ARGS="BLD_OW19_WORKAROUND=1"

# *** *** *** *** ***

export PATH="$WATCOMDIR"/"$WATCOMPLATFORM":"$NASMDIR":$PATH
export INCLUDE="$WATCOMDIR"/h
export WATCOM="$WATCOMDIR"
export EDPATH="$WATCOM"/eddat

export OUTDIR="$OUTSRCDIR"/OBJ

# *** *** Build *** ***

cd "$OUTDIR"

wmake -f ../KDRLINUX.MK "$WMAKE_ARGS"
